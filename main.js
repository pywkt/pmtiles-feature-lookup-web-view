import maplibregl from 'maplibre-gl';
import * as pmtiles from 'pmtiles';
import mapStyle from './styles/style.json';
import './styles/style.css';

const TILE_URL = import.meta.env.VITE_TILE_URL;

const url = new URL(window.location.href.toLowerCase());
const lon = parseFloat(url.searchParams.get('lon'));
const lat = parseFloat(url.searchParams.get('lat'));

if (!lon || !lat) {
  throw new Error('nothing');
}

const protocol = new pmtiles.Protocol();
maplibregl.addProtocol('pmtiles', protocol.tile);

const mapContainer = document.createElement('div');

const map = new maplibregl.Map({
  container: mapContainer,
  // Uncomment this and element in index.html to view map
  // container: 'map',
  style: {
    ...mapStyle,
    sources: {
      basemap: {
        type: 'vector',
        url: `pmtiles://${TILE_URL}`,
      },
    },
    zoom: 20,
    center: [lon, lat],
  },
});

map.on('load', (e) => {
  const features = map.queryRenderedFeatures(e.point);
  if (features.length > 0) {
    console.log(features[0].properties.name);
  }
});

map.addControl(new maplibregl.NavigationControl(), 'top-left');
