## Setup

- `git clone https://gitlab.com/pywkt/pmtiles-lookup-web`
- `cd pmtiles-lookup-web && npm install`
- `echo "VITE_TILE_URL=<url-to-pmtiles-file>" >> .env`
- `npm run dev`

## Run

Open the browser console and navigate to `http://localhost:5173/?lon=<longitude>&lat=<latitude>`

## Notes

This has a few differences from the [Node Package](https://gitlab.com/pywkt/pmtiles-feature-lookup):

- Much more precise
  - Instead of being limited by the zoom level baked in to the .pmtiles file, maplibregl can "overzoom" and then center the map/cursor position. This allows for a more accurate lookup.

- Does not run on the server
  - Unfortunately the maplibregl-js package requires a "desktop" environment so attempting to run in an environment like NodeJS will error.
  - The maplibregl-native package can be manipulated to think there is a display, but it's only supported up to Node 16 and still has complications. This should be looked in to more.
